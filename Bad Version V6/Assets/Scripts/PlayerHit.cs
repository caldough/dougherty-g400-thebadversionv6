using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    public AudioSource hitWall;
    public AudioSource reachedEnd;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            hitWall.Play();
        }

        if (collision.gameObject.CompareTag("End"))
        {
            reachedEnd.Play();
        }
    }
}
